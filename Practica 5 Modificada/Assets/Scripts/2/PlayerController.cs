﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

    public WaypointController waypointControl;      // Reference to WaypointController Script
    public float speed = 5;
    private GameObject cam;

    private void Start() {
        cam = GameObject.Find("Main Camera");      // Get GameObject reference by Name
    }

	void Update () {
        float step = speed * Time.deltaTime;    // Unity units per seconds
        Vector3 currentWaypointPos = waypointControl.getCurrentWaypointPos();

        // Move player's transform to currentWaypoint position
        transform.position = Vector3.MoveTowards(transform.position, currentWaypointPos, step);

        //Move the camera and the player together
        cam.transform.position =  transform.position - Vector3.forward * 10f;
    }
}


