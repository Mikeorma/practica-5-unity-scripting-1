﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cosadecolor : MonoBehaviour {
    public bool colorsico = true;
    private float ojetecalor;
    public Color colorparacambiar = Color.blue;
    public bool cosarandom = false;
    private float ojetecalor2;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {



        if (colorsico == true && cosarandom != true)
        {
            if (Input.GetKey(KeyCode.C))
            {

                GetComponent<Renderer>().material.color = colorparacambiar;
            }
            else
            {
                GetComponent<Renderer>().material.color = Color.green;
            }
        }



        else if (colorsico != true && cosarandom != true)
        {
            ojetecalor = Input.GetAxisRaw("Color");



            if (ojetecalor != 0)
            {
                GetComponent<Renderer>().material.color = colorparacambiar;
            }

            else if (ojetecalor == 0)
            {
                GetComponent<Renderer>().material.color = Color.green;
            }


        }



        else if (cosarandom == true)
        {
            ojetecalor2 = Input.GetAxisRaw("Color");

            Color colorrandom = new Color(Random.value, Random.value, Random.value, 1.0f);

            if (ojetecalor2 != 0)
            {
                GetComponent<Renderer>().material.color = colorrandom;
            }
        }
    }
}
