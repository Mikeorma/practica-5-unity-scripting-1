﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cosadeescala : MonoBehaviour {

    public float escala = 0.1f;
    public bool teclac = true;
    public bool ctrol = false;
    public bool randomeje = false;
    private float escalita;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (teclac == true)
        {
            if (Input.GetKey(KeyCode.G))
            {
                transform.localScale += new Vector3(escala, escala, escala);
            }

            else
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
        }

        else if (ctrol == true)
        {
            escalita = Input.GetAxisRaw("Escala");



            if (escalita != 0)
            {
                GetComponent<Transform>().localScale += new Vector3(escala, escala, escala);
            }

            else if (escalita == 0)
            {
                GetComponent<Transform>().localScale = new Vector3(1,1,1);
            }
        }

        else if (randomeje == true)
        {
            if (Input.GetKey(KeyCode.T))
            {
                transform.localScale = new Vector3(Random.Range(0,5), Random.Range(0, 5), Random.Range(0, 5));
            }
        }
		
	}
}
